## 1.79 / 3.23

- 1.79.37 - Возвращена стандартная иконка приложения [JURIDICAL-31862](https://jira.psbnk.msk.ru/browse/JURIDICAL-31862)
- 1.79.36 - Новая версия [JURIDICAL-31861](https://jira.psbnk.msk.ru/browse/JURIDICAL-31861)
- 1.79.35 - Возвращена стандартная иконка приложения [JURIDICAL-31862](https://jira.psbnk.msk.ru/browse/JURIDICAL-31862)
- 1.79.34 - Возвращена стандартная иконка приложения [JURIDICAL-31862](https://jira.psbnk.msk.ru/browse/JURIDICAL-31862)
- 1.79.33 - Возвращена стандартная иконка приложения [JURIDICAL-31862](htttps://jira.psbnk.msk.ru/browse/JURIDICAL-31862)

## 1.78 / 3.22

- 1.78.100 - Возвращена стандартная иконка приложения [JURIDICAL-31862](https://jira.psbnk.msk.ru/browse/JURIDICAL-31862)
- 1.78.099 - Возвращена стандартная иконка приложения [JURIDICAL-31862](https://jira.psbnk.msk.ru/browse/JURIDICAL-31862)
