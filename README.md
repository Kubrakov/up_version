# up_version

## about

скрипт увеличивает версию в changelog.md при каждом merge в master а также меняет версию на актуальную в project.pbxproj или в build.gradle (скрипт написан для обоих файлов). Changelog.md также двух типов (разные команды используют разную структуру файла)

Также если в коммите добавить [параметр], например [1.78] то коммит запишется в changelog.md  в соответствующую версию приложения

### что нужно сделать чтобы сработало

settings --> repository --> deploy tokens --> expand генерим токен и даем ему права чтобы пушить в репо в мастер

settings --> repository --> protected branches --> expand проверяем и по необходимости добавляем кто может пушить и мерджить в мастер
