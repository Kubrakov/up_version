import re
import sys


def define_current_version(input_file, ver):
    regexp = r"(\+\s\d{1,}.\d{1,}.\d{1,})"  # example is inside brackets: [+ 1.78.10]
    with open(input_file, 'r') as file:
        for line in file:
            match = re.match(regexp, line)
            if match is not None:
                ver = line[2:-1]
                break
    return ver


def define_new_version(ver):
    count_of_chars_string_version = len(ver)
    count_of_chars_string_version_before_third_digit = ver.rfind('.') + 1
    third_digit_of_version = int(ver[
                                 count_of_chars_string_version_before_third_digit:count_of_chars_string_version])
    new_third_digit_of_version = third_digit_of_version + 1
    new_ver = ver[:count_of_chars_string_version_before_third_digit] + str(new_third_digit_of_version)
    return new_ver


def increment_version_in_changelog(input_file, line_num, new_ver, com):
    clean_comment = com[:position_of_text_see_merge_request - 1]

    lines_to_add = f'+ {new_ver}\n    + {clean_comment}\n\n'

    with open(input_file, 'r') as file:
        contents = file.readlines()

    contents.insert(line_num, lines_to_add)

    with open(input_file, 'w') as file:
        contents = ''.join(contents)
        file.write(contents)


def replace_version_in_pbxproj(input_file, ver, new_ver):
    with open(input_file, 'r') as file:
        old_data = file.read()
    new_data = old_data.replace(ver, new_ver)
    with open(input_file, 'w') as file:
        file.write(new_data)


def write_version_to_txt_file(input_file, new_ver, com):
    task_number = com[:position_of_closed_bracket + 1]

    line_to_add = f'{task_number} version {new_ver}'

    with open(input_file, 'w') as file:
        file.write(line_to_add)


read_file = 'CHANGELOG.md'
write_file = 'PSB.xcodeproj/project.pbxproj'
version_file = 'version.txt'
line_number = 4
substring_in_comment = 'See merge request'
closed_bracket = ']'

if __name__ == '__main__':
    comment = ' '.join(sys.argv[6:])

    version = 0
    version = define_current_version(read_file, version)

    if version == 0:
        print('версию не удалось определить')
        # todo отправить в error_log или написать на почту
    else:
        new_version = define_new_version(version)

        try:
            position_of_text_see_merge_request = comment.index(substring_in_comment)
            position_of_closed_bracket = comment.index(closed_bracket)
        except ValueError:
            print("Подстрока See merge request в коммите не найдена! или закрывающаяся скобка не найдена")
        else:
            increment_version_in_changelog(read_file, line_number, new_version, comment)

            replace_version_in_pbxproj(write_file, version, new_version)

            write_version_to_txt_file(version_file, new_version, comment)
