import re
import sys
import os

changelog_md = 'CHANGELOG.md'
build_gradle = 'application/build.gradle'
version_file = 'version.txt'
jira_reference = 'https://jira.psbnk.msk.ru/browse/'


def define_clean_comment_and_params(com):
    positions_of_open_brackets = [m.start() for m in re.finditer('\[', com)]
    positions_of_close_brackets = [m.start() for m in re.finditer(']', com)]
    position_of_text_see_merge_request = com.find('See merge request')
    if position_of_text_see_merge_request == -1:
        raise ValueError('Не найдена подстрока See merge request')
    params = []
    count_of_close_brackets = len(positions_of_close_brackets)
    count_of_open_brackets = len(positions_of_open_brackets)
    if count_of_close_brackets != count_of_open_brackets:
        raise ValueError('Не совпадает количество открывающихся и закрывающихся скобок')
    positions_of_text_juridical = [m.start() for m in re.finditer('JURIDICAL-', com)]
    count_of_text_juridical = len(positions_of_text_juridical)
    position_of_text_closes_juridical = com.find('Closes JURIDICAL-')
    if count_of_text_juridical != 1:
        if position_of_text_closes_juridical == -1:
            raise ValueError('В коммите должна быть ровно одна подстрока JURIDICAL-. Не больше и не меньше. '
                             'Файлы не буду инкрементированы')

    position_of_text_juridical = positions_of_text_juridical[0]
    if count_of_open_brackets != 0:
        positions_of_brackets = [positions_of_open_brackets, positions_of_close_brackets]
        positions_of_data_inside_brackets = list(zip(*positions_of_brackets))
        position_of_last_close_bracket = positions_of_data_inside_brackets[-1][1]
        if position_of_text_juridical < position_of_last_close_bracket:
            raise ValueError('Все параметры в скобках должны быть до подстроки JURIDICAL-')
        for pos in positions_of_data_inside_brackets:
            params.append(com[pos[0] + 1:pos[1]])

    if position_of_text_closes_juridical == -1:
        cl_comment = com[position_of_text_juridical:position_of_text_see_merge_request - 1]
    else:
        cl_comment = com[position_of_text_juridical:position_of_text_closes_juridical - 1]

    return cl_comment, params

def analyse_parameters(params):
    major_num = 0
    minor_num = 0
    for param in params:
        positions_of_dot = [m.start() for m in re.finditer('\.', param)]
        count_of_dots_in_param = len(positions_of_dot)
        if count_of_dots_in_param == 1:
            string_before_dot = param[:positions_of_dot[0]]
            string_after_dot = param[positions_of_dot[0] + 1:]
            if string_before_dot.isdigit() and string_after_dot.isdigit():
                major_num = string_before_dot
                minor_num = string_after_dot
    if major_num == 0 and minor_num == 0:
        print('Для справки. Параметры были. Но версии среди них не найдено')
    return major_num, minor_num

def define_current_version(input_file, major_num=0, minor_num=0):
    ver = 0
    line_num = 0
    os.chdir(os.getcwd())
    if major_num == 0 and minor_num == 0:
        regexp = r"(\-\s\d{1,}.\d{1,}.\d{1,})"  # last version. example is: - 1.80.015
    else:
        regexp = r"\-\s" + f'{major_num}.{minor_num}' + r".\d{1,}"  # version in param. example is: - 1.79.015
    with open(input_file, 'r') as file:
        for line_num, line in enumerate(file):
            match = re.search(regexp, line)
            if match is not None:
                position_of_last_symbol_of_ver = match.span()[1]
                ver = line[2:position_of_last_symbol_of_ver]
                break
    return ver, line_num


def define_new_version(ver):
    count_of_chars_string_version = len(ver)
    count_of_chars_string_version_before_third_digit = ver.rfind('.') + 1
    third_digit_of_version = int(ver[
                                 count_of_chars_string_version_before_third_digit:count_of_chars_string_version])
    new_third_digit_of_version = third_digit_of_version + 1
    new_ver = ver[:count_of_chars_string_version_before_third_digit] + str(new_third_digit_of_version).zfill(3)
    return new_ver

def increment_version_in_changelog(input_file, new_ver, cl_comment, jira_ref, line_num):
    regexp = r"(JURIDICAL-\d{4,5}\s)"
    match = re.search(regexp, cl_comment)
    if match is not None:
        position_of_last_symbol_of_regexp = match.span()[1]
        task_num = cl_comment[:position_of_last_symbol_of_regexp - 1]
        cl_comment_without_task_num = cl_comment[position_of_last_symbol_of_regexp:]
        line_to_add = f'- {new_ver} - {cl_comment_without_task_num} [{task_num}]({jira_ref}{task_num})\n'
    else:
        raise ValueError('Между JURIDICAL-XXXXX и остальным коммитом нет пробела')
    os.chdir(os.getcwd())
    with open(input_file, 'r') as file:
        contents = file.readlines()

    contents.insert(line_num, line_to_add)

    with open(input_file, 'w') as file:
        contents = ''.join(contents)
        file.write(contents)
    return task_num

def replace_version_in_build_grandle(input_file, new_ver):
    with open(input_file, 'r') as file:
        old_data = file.read()

    first_digit_of_new_version = new_ver[:new_ver.find('.')]
    first_digit_of_new_version_int = int(first_digit_of_new_version)

    second_digit_of_new_version = new_ver[new_ver.find('.') + 1:new_ver.rfind('.')]
    second_digit_of_new_version_int = int(second_digit_of_new_version)

    third_digit_of_new_version = new_ver[new_ver.rfind('.') + 1:]
    third_digit_of_new_version_int = int(third_digit_of_new_version)

    list_of_finding_strings = ['def versionMajor', 'def versionMinor', 'def versionBuild']
    list_of_lines_to_replace = []
    with open(input_file, 'r') as file:
        for line in file:
            for regexp in list_of_finding_strings:
                match = re.search(regexp, line)
                if match is not None:
                    list_of_lines_to_replace.append(line)
    string_to_replace = ''.join(list_of_lines_to_replace)
    new_data = old_data.replace(string_to_replace, f'    def versionMajor = {first_digit_of_new_version_int}\n    '
                                                   f'def versionMinor = {second_digit_of_new_version_int}\n    '
                                                   f'def versionBuild = {third_digit_of_new_version_int}\n')
    with open(input_file, 'w') as file:
        file.write(new_data)


def write_version_to_txt_file(input_file, new_ver, task_num):
    line_to_add = f'{task_num} version {new_ver} '
    with open(input_file, 'w') as file:
        file.write(line_to_add)


if __name__ == '__main__':
    comment = ' '.join(sys.argv[6:])

    clean_comment, parameters = define_clean_comment_and_params(comment)

    if parameters:
        major_number, minor_number = analyse_parameters(parameters)
        version, line_number_where_match = define_current_version(changelog_md, major_number, minor_number)
    else:
        version, line_number_where_match = define_current_version(changelog_md)

    if version == 0:
        raise ValueError('версию не удалось определить')
    else:
        new_version = define_new_version(version)
        task_number = increment_version_in_changelog(changelog_md, new_version, clean_comment, jira_reference,
                                                     line_number_where_match)
        replace_version_in_build_grandle(build_gradle, new_version)
        write_version_to_txt_file(version_file, new_version, task_number)
